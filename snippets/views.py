# from django.shortcuts import render
from tkinter.messagebox import NO
from .models import Well, Employee
# from rest_framework import permissions
from rest_framework.generics import GenericAPIView
from django.db import models
from rest_framework.response import Response
from rest_framework import status
from rest_framework import serializers
from .serializers import WellSerializer, EmployeeSerializer
from rest_framework.exceptions import APIException


# Create your views here.
class WellList(GenericAPIView):
    """
    List all snippets, or create a new snippet.
    """
    def get(self, request, format=None):
        query = Well.objects.all()
        serializer = WellSerializer(query, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = WellSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class EmployeeList(GenericAPIView):
    """
    List all snippets, or create a new snippet.
    """
    def get(self, request, format=None):
        query = Employee.objects.all()
        serializer = EmployeeSerializer(query, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = EmployeeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CreateAndUpdateAPIView(GenericAPIView):
    model: models.Model = None
    serializer_class: serializers.ModelSerializer = None
    key_field: str = ''

    def get_object(self, key):
        try:
            return self.model.objects.get(**{self.key_field: key})
        except self.model.DoesNotExist:
            return None
        except self.model.MultipleObjectsReturned:
            raise APIException("More than 2 objects with this key")

    def update_db(self, object, data):
        serializer = self.serializer_class(object, data=data)
        if serializer.is_valid():
            serializer.save()
        return Response({'message': 'Data processed'})

    def post(self, request, format=None):
        try:
            search_object = self.get_object(request.data[self.key_field])
            return self.update_db(search_object, request.data)
        except Exception as e:
            raise APIException("Server error")

class EmployeeCreateAndUpdate(CreateAndUpdateAPIView):
    model = Employee
    serializer_class = EmployeeSerializer
    key_field = 'name'

class WellCreateAndUpdate(CreateAndUpdateAPIView):
    model = Well
    serializer_class = WellSerializer
    key_field = 'nickname'