from django.db import models

# Create your models here.

class Well(models.Model):
    name = models.CharField(max_length=200)
    nickname = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    performance = models.IntegerField(default=0)
    # class Meta:
    #     verbose_name = "Well"

class Employee(models.Model):
    name = models.CharField(max_length=200)
    surname = models.CharField(max_length=200)
    position = models.CharField(max_length=200)
    age = models.IntegerField(default=0)
    # class Meta:
    #     verbose_name = "Employee"