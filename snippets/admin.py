from django.contrib import admin

from .models import Well, Employee

# Register your models here.

admin.site.register(Well)
admin.site.register(Employee)
