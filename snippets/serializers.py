from .models import Well, Employee
from rest_framework import serializers


class WellSerializer(serializers.ModelSerializer):
    class Meta:
        model = Well
        fields = ['name', 'nickname', 'description', 'performance']

class WellSilpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Well
        fields = ['name', 'description']


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = ['name', 'surname', 'position', 'age']

class EmployeeSilpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = ['name', 'surname']

